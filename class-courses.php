<?php

	require __DIR__.'/vendor/autoload.php';

	use Kreait\Firebase\Factory;
	use Kreait\Firebase\ServiceAccount;

	class LearnSnip_Courses{

		private $filename;
		private $serviceAccount;
		private $firebase;
		private $database;
		public $id;

		public function __construct(){
			$this->filename = __DIR__.'/LearnSnipApp-e624262bdfc7.json';
			$this->id = 
			$this->serviceAccount = ServiceAccount::fromJsonFile($this->filename);
			$this->apiKey = 'e624262bdfc796f3b4f53c887d7a832cc6bff9d6';


			$this->firebase = (new Factory)
			    ->withServiceAccountAndApiKey($this->serviceAccount, $this->apiKey)
			    ->create();
		   	$this->database = $this->firebase->getDatabase();
		}


		public function getCourses($user){
			$this->id = $user;
			$reference = $this->database->getReference('courses');
			$snapshot = $reference->getSnapshot();
			$courses = $snapshot->getValue();
			
			return $this->filterCourses($courses);
		}

		public function filterCourses($data){
			$valid_courses =  array();
			foreach ($data as $key => $value) {
				if($value['owner'] == $this->id){
					$valid_courses[$key] = $value;
				}
			}
			return $valid_courses;
		}

	}






?>