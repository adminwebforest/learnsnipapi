<?php

	require __DIR__.'/vendor/autoload.php';



	require 'path/to/PHPMailer/src/Exception.php';
	require 'path/to/PHPMailer/src/PHPMailer.php';
	require 'path/to/PHPMailer/src/SMTP.php';

	use Kreait\Firebase\Factory;
	use Kreait\Firebase\ServiceAccount;
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	class LearnSnip_Notifications{

		private $filename;
		private $serviceAccount;
		private $firebase;
		private $database;
		public $id;

		public function __construct(){
			
		}

		public function send_simple_email($subject, $message, $from, $to){

			$mail = new PHPMailer(true); 
			try {
    		//Server settings
		    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
		    $mail->isSMTP();                                      // Set mailer to use SMTP
		    $mail->Host = 'secure.emailsrvr.com';  // Specify main and backup SMTP servers
		    $mail->SMTPAuth = true;                               // Enable SMTP authentication
		    $mail->Username = 'accounts@kudosable.com';                 // SMTP username
		    $mail->Password = 'Pa55w0rd123';                           // SMTP password
		    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = 465;                                    // TCP port to connect to

		    //Recipients
		    $mail->setFrom($from, 'LearnSnip WP');
		    $mail->addAddress($to, '');     // Add a recipient
		    //Content
		    $mail->isHTML(true);                                  // Set email format to HTML
		    $mail->Subject = $subject;
		    $mail->Body    = $message;
		    $mail->AltBody = $message;

		    $mail->send();
		    return 'Message has been sent to' . $to;
		} catch (Exception $e) {
		    return 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
		}
		}
		

	}






?>