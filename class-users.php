<?php

	require __DIR__.'/vendor/autoload.php';

	use Kreait\Firebase\Factory;
	use Kreait\Firebase\ServiceAccount;

	class LearnSnip_Users{

		private $filename;
		private $serviceAccount;
		private $firebase;
		private $database;
		public $id;

		public function __construct(){
			$this->filename = __DIR__.'/LearnSnipApp-e624262bdfc7.json';
			$this->id = 
			$this->serviceAccount = ServiceAccount::fromJsonFile($this->filename);
			$this->apiKey = 'e624262bdfc796f3b4f53c887d7a832cc6bff9d6';


			$this->firebase = (new Factory)
			    ->withServiceAccountAndApiKey($this->serviceAccount, $this->apiKey)
			    ->create();
		   	$this->database = $this->firebase->getDatabase();
		}


		public function updateUserProgress($data){
			$reference = $this->database->getReference('wp_students/'.$data['uid'].'/'. $data['id'] .'/courses/'. $data['cKey'].'/lessons/'.$data['lKey'].'/progress');
			$reference->set($data['progress']);

			$this->updateSession($data);
			//return $data;
		}

		public function updateSession($data){
			$reference = $this->database->getReference('wp_students/'.$data['uid'].'/'. $data['id'] .'/courses/'. $data['cKey'] .'/lastSession');
			$reference->set($data['session']);
		}

		public function setUser($uid, $id, $name){
			$reference = $this->database->getReference('wp_students/'.$uid.'/'. $id .'/name');
			$reference->set($name);
		}

		public function getUserData($userData){

			$reference = $this->database->getReference('wp_students/'.$userData['uid'].'/'. $userData['id']);
			$snapshot = $reference->getSnapshot();
			$user = $snapshot->getValue();
			
			return $user;
		}

		public function sanitizeUser($data){
			$user_data =  array();
			$user_data['points'] = $user['points'];
			foreach ($data['courses'] as $key => $course) {
				$user_data['courses'][$key] = $course;
			}

			return $user_data;
		}

		public function sanitizeLessons($data){
			$data =  array();
			foreach ($data as $key => $lesson) {
				$data['lessons'][$key] = $lesson;
			}
			return $$data;
		}

		public function updateUserPoints($data){
			$reference = $this->database->getReference('wp_students/'.$data['uid'].'/'. $data['id'] .'/points');
			$reference->set($data['points']);
		}

		public function addNote($data){
			$reference = $this->database->getReference('wp_students/'.$data['uid'].'/'. $data['id'] .'/courses/'. $data['cKey'].'/lessons/'.$data['lKey'].'/notes');
			if(count($data['notes']) > 0){
				$reference->set($data['notes']);
			} else {
				$reference->remove();
			}
		}

		public function getAllUsers($id){

			$reference = $this->database->getReference('wp_students/'. $id);
			$snapshot = $reference->getSnapshot();
			$users = $snapshot->getValue();
			
			return $users;
		}

	}






?>