<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');

include("class-courses.php");
include("class-users.php");
include("class-notifications.php");
require __DIR__.'/vendor/autoload.php';


use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
$filename = __DIR__.'/LearnSnipApp-e624262bdfc7.json';

$serviceAccount = ServiceAccount::fromJsonFile($filename);
$apiKey = 'e624262bdfc796f3b4f53c887d7a832cc6bff9d6';


$firebase = (new Factory)
    ->withServiceAccountAndApiKey($serviceAccount, $apiKey)
    ->create();

$courses = new LearnSnip_Courses();
$users = new LearnSnip_Users();
$notify = new LearnSnip_Notifications();

if(isset($_POST['code'])){
	$data = array();
	$database = $firebase->getDatabase();
	$id = substr($_POST['code'],13);
	$reference = $database->getReference('publishers/'.$id);

	$snapshot = $reference->getSnapshot();
	$publisher = $snapshot->getValue();

	if($publisher['access_code'] == $_POST['code']){
		$data['status'] = 'granted';
		$userCourses = $courses->getCourses($id);
		$data['courses'] = $userCourses;
	} else{
		$data['status'] = 'denied';
	}
	$result = json_encode($data);
	echo $result;

} else if(isset($_POST['request'])){

	if($_POST['request'] == 'ls01'){
		$progressData = array(
				'id' => $_POST['id'],
				'uid' => $_POST['uid'],
				'session' => $_POST['session'],
				'cKey' => $_POST['cKey'],
				'lKey' => $_POST['lKey'],
				'progress' => $_POST['progress']
			);

			$users->updateUserProgress($progressData);
			$users->setUser($_POST['uid'], $_POST['id'], $_POST['name']);
			$data = 'OKAY';
	} else if($_POST['request'] == 'ls02'){
		$userData = array('uid' => $_POST['uid'], 'id' => $_POST['id']);
		$data = $users->getUserData($userData);
	} else if($_POST['request'] == 'ls03'){
		$userData = array('uid' => $_POST['uid'],'id' => $_POST['id'], 'points' => $_POST['points']);
		$users->updateUserPoints($userData);
		$data = 'OKAY';
	} else if($_POST['request'] == 'ls04'){
		$noteData = array(
				'id' => $_POST['id'],
				'uid' => $_POST['uid'],
				'notes' => $_POST['notes'],
				'cKey' => $_POST['cKey'],
				'lKey' => $_POST['lKey']
			);
		$users->addNote($noteData);
		$data = 'OKAY';
	}else if($_POST['request'] == 'ls05'){
	
		$data = $users->getAllUsers($_POST['uid']);
	} else if($_POST['request'] == 'ls06'){
		
		$data = $users->getAllCourses();
	} else if($_POST['request'] == 'ls07'){
		
		$data = $courses->getCourses($_POST['uid']);
	}else if($_POST['request'] == 'ls08'){
		
		$data = $notify->send_simple_email($_POST['subject'], $_POST['subject'], $_POST['from'], $_POST['to']);
	}

			

	$result = json_encode($data);
	echo $result;


} else {
	echo 'YOU\'RE NOT ALLOWED HERE SILLY HUMAN';
}



